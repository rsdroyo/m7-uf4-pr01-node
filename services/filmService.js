const constants = require('../config/constants');
const Film = require('../models/db/filmModel');
const crudRepository = require('../database/crudRepository');
const mongoose = require('mongoose');

module.exports.createFilm = async function(film){
    const responseObj = constants.responseObj;
    try{
        const data = {
            model: new Film(film)
        }
        const responseFromDatabase = await crudRepository.insertData(data);

        if (responseFromDatabase.status) {
            responseObj.status = constants.httpStatus.created;
            responseObj.body = responseFromDatabase.result;
        }
        
    }catch (e){
        console.error('ERROR-filmService-createFilm', e);
    }
    return responseObj;
}

module.exports.updateFilm = async function(film){
    const responseObj = constants.responseObj;
    try{
        const data = {
            findQuery: {
                _id: mongoose.Types.ObjectId(film.id)
            },
            model: Film,
            projection: {
                _id: false
            },
            updateQuery: {}
        };
        Object.keys(film).forEach(key => {
            if (key !== 'id' || key !== '_id'){
                data.updateQuery[key] = film[key];
            }
        })


        const responseFromDatabase = await crudRepository.updateData(data);

      
        if (responseFromDatabase.status) {
            responseObj.status = constants.httpStatus.ok;
            responseObj.message = constants.controllerMessages.filmUpdate;
            responseObj.body = responseFromDatabase.result;
        }
        
    }catch (e){
        console.error('ERROR-filmService-updateFilm', e);
    }

    return responseObj;
}