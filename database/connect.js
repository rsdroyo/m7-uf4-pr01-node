const mongoose = require('mongoose');

module.exports.createConnection = () =>{
    const myPromise = new Promise((resolve, reject) => {
        mongoose.connect(process.env.DB_URL, {useNewUrlParser: true, useUnifiedTopology: true}, (err) => {
            if (err) {
                console.log('ERROR DB CONNECTION');
                return reject('ERROR DB CONNECTION');
            }else{
                console.log('DB CONNECTED');
                return resolve('DB CONNECTED');
            }
        });
    });
    return myPromise;
}