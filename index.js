const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const connect = require('./database/connect');
const routesPractica = require('./routes/routesPractica');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

async function connectDB() {
    try {
        await connect.createConnection();
    } catch (e) {
        console.log('ERROR-creteConnection', e);
    }
}
    connectDB();

    app.use('/api/v1/film/', routesPractica);

    app.listen(process.env.PORT, function(){
        console.log('Server start!',
        `Example app listening on port ${process.env.PORT}`);
    })
